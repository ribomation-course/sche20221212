import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CssClassComponent } from './css-class/css-class.component';
import { CssPropertyComponent } from './css-property/css-property.component';
import { TemplateComponent } from './template/template.component';
import { UsingIfComponent } from './using-if/using-if.component';
import { UsingSwitchComponent } from './using-switch/using-switch.component';
import { UsingForComponent } from './using-for/using-for.component';

@NgModule({
  declarations: [
    AppComponent,
    CssClassComponent,
    CssPropertyComponent,
    TemplateComponent,
    UsingIfComponent,
    UsingSwitchComponent,
    UsingForComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
