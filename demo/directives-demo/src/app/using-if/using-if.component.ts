import { Component } from '@angular/core';

@Component({
  selector: 'app-using-if',
  templateUrl: './using-if.component.html',
  styleUrls: ['./using-if.component.css']
})
export class UsingIfComponent {
  authenticated: boolean = false;
  user: any;

  toggleAuthenticated() {
    if (this.authenticated) {
      this.user = undefined;
      this.authenticated = false;
    } else {
      this.user = { name: 'Anna Conda', email: 'anna@gmail.com' };
      this.authenticated = true;
    }
  }
}


