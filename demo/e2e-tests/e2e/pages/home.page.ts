import { browser, by, element } from 'protractor';

export class HomePage {
  navigateTo() {
    return browser.get('/home');
  }

  getHeaderText() {
    return element(by.css('app-home h1')).getText();
  }

}
