import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  name: string = 'Simple e2e Demo';
  version:number=1;
  date:Date = new Date();
  author: string = 'Jens Riboe';
  organization: string = 'Ribomation';
  url: string = 'https://www.ribomation.se/';

  constructor() { }

  ngOnInit() {
  }

}
