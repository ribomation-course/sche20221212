import {BrowserModule}                    from '@angular/platform-browser';
import {NgModule}                         from '@angular/core';
import {Route, RouterModule}              from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppComponent}               from './app.component';
import {TemplateOrientedComponent}  from './template-oriented/template-oriented.component';
import {ComponentOrientedComponent} from './component-oriented/component-oriented.component';
import { MultipleOfDirective }      from './validators/multiple-of.directive';
import { EqualsDirective } from './validators/equals.directive';
import { InteractiveFormFieldComponent } from './interactive-form-field/interactive-form-field.component';

const routes: Route[] = [
  {path: 'template-oriented', component: TemplateOrientedComponent},
  {path: 'component-oriented', component: ComponentOrientedComponent},
  {path: 'interactive-form-field', component: InteractiveFormFieldComponent},
  {path: '', redirectTo: '/', pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    TemplateOrientedComponent,
    ComponentOrientedComponent,
    MultipleOfDirective,
    EqualsDirective,
    InteractiveFormFieldComponent
  ],
  imports:      [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers:    [],
  bootstrap:    [AppComponent]
})
export class AppModule {}
