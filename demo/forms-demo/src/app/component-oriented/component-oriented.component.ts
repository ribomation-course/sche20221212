import {
  Component,
  OnInit
} from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {
  equals,
  multipleOf
} from '../validators/user-validators';

@Component({
  selector:    'app-component-oriented',
  templateUrl: './component-oriented.component.html',
  styleUrls:   ['../app.component.css']
})
export class ComponentOrientedComponent implements OnInit {
  form: FormGroup;

  constructor(private formBldr: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.formBldr.group({
      name:   ['', [Validators.required, Validators.minLength(3), Validators.maxLength(10)]],
      age:    ['', [Validators.required, multipleOf(4), Validators.min(1), Validators.max(100)]],
      email:  ['', [Validators.required, Validators.email, Validators.minLength(5)]],
      email2: ['', [Validators.required]],
    }, {validators: equals('email', 'email2')});
  }

  get f(): any {
    return this.form.controls;
  }

  onSubmit() {
    alert('Profile submitted: ' + JSON.stringify(this.form.value));
    this.form.reset();
  }
}
