import {Component} from "@angular/core";
import {AbstractControl, FormControl, FormGroup, ValidatorFn, Validators} from "@angular/forms";

@Component({
  selector: "model-driven-form",
  template: `
    <h2>Model-Driven Form</h2>
    <form novalidate [formGroup]="form" (ngSubmit)="onSubmit()">
      <table>
        <tr><th>Product Name</th>
          <td><input type="text" formControlName="productName"/></td>
        </tr>
        <tr><th>Price</th>
          <td><input type="number" formControlName="price" min="0"/></td>
        </tr>
        <tr><th></th>
          <td>
            <button type="submit">SEND</button>
            <button type="button" (click)="onReset()">Clear</button>
          </td>
        </tr>
      </table>
    </form>
    <pre>DATA [{{form.status}}]: {{form.value | json}}</pre>
    <pre>name.err: {{productName.errors | json}}</pre>
    <pre>price.err: {{price.errors | json}}</pre>
  `,
})
export class ModelDrivenFormComponent {
  form: FormGroup;
  productName: FormControl;
  price: FormControl;

  constructor() {
    this.productName = new FormControl("a", [
      Validators.required,
      Validators.minLength(5),
      Validators.maxLength(20),
    ]);

    this.price = new FormControl(0, [
      Validators.required,
      Validators.min(1),
      Validators.max(1000),
      this.multipleOf(4)
    ]);

    this.form = new FormGroup({
      productName: this.productName,
      price: this.price
    });
  }

  onSubmit(): void {
    if (this.form.valid) {
      console.log("[model-driven]", "form data", this.form.value);
      this.onReset();
    } else {
      console.log("[model-driven]", "form data invalid");
    }
  }

  onReset(): void {
    //this.form.reset();
    this.productName.setValue("p");
    this.price.setValue(42);
  }

  multipleOf(n: number): ValidatorFn {
    return (field: AbstractControl): { [err: string]: any } => {
      const result = field.value % n;
      return (result === 0)
        ? null
        : {multipleOf: {actual: field.value, multiple: n, result: result}};
    };
  }
}
