import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import { ComponentOrientedFormComponent } from './component-oriented-form/component-oriented-form.component';
import { TemplateOrientedFormComponent } from './template-oriented-form/template-oriented-form.component';

const routes: Route[] = [
  {path: 'component-oriented', component: ComponentOrientedFormComponent},
  {path: 'template-oriented', component: TemplateOrientedFormComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
