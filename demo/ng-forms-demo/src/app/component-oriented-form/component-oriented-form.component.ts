import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Product } from '../product.model';

@Component({
  selector: 'app-component-oriented-form',
  templateUrl: './component-oriented-form.component.html',
  styleUrls: ['./component-oriented-form.component.scss']
})
export class ComponentOrientedFormComponent implements OnInit {
  nameCtrl: FormControl;
  priceCtrl: FormControl;
  productForm: FormGroup;

  ngOnInit() {
    this.nameCtrl = new FormControl('Banana');
    this.priceCtrl = new FormControl(42);
    this.productForm = new FormGroup({
      name: this.nameCtrl,
      price: this.priceCtrl
    });
  }

  updateProductName() { this.nameCtrl.setValue('Orange'); }
  updateProductPrice() { this.productForm.patchValue({ price: 100 }); }
  onSubmit() {
    const payload: Product = this.productForm.value as Product;
    this.productForm.reset({ name: 'Apple', price: 17 });

    alert(`payload: ${JSON.stringify(payload)}`);
  }

}
