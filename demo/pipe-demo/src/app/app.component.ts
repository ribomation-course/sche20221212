import {Component} from '@angular/core';

import '@angular/common/locales/global/sv'


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    user: any = {
        name: {
            first: 'Anna', last: 'Conda'
        },
        email: 'anna.conda@gmail.com',
        tags: [
            'angular', 'node', 'scss'
        ]
    };

    text = 'TJOLLAhopp Software Ltd.';

    value = 4 * Math.atan(1);

    today = new Date(2022, 11, 24, 15, 0);
    fmt = 'd MMM yyyy, HH:mm';

    list = 'ABCDEF'.split('');

}
