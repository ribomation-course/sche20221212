import {Pipe, PipeTransform} from '@angular/core';

function isEmpty(s: string): boolean {
    return s === undefined || s === null || s.trim().length === 0;
}

function toTitle(s: string): string {
    return s[0].toUpperCase() + s.substring(1).toLowerCase();
}

@Pipe({name: 'train'})
export class TrainCasePipe implements PipeTransform {
    transform(value: string, use_dash: boolean = true): string {
        if (isEmpty(value)) return '';
        return value
            .replace(/[^a-z ]/gi, '')
            .split(/\s+/)
            .map(word => toTitle(word))
            .join(use_dash ? '-' : '_');
    }
}

