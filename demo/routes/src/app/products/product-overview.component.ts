import { Component} from '@angular/core';

@Component({
  template: `
    <div>
      <h3>Overview of THE Product</h3>
      <p>This is just an awesome product. 
        Just one of a kind and a must-have.</p>
    </div>
  `,
})
export class ProductOverviewComponent {}
