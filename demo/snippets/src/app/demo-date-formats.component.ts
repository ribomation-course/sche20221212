import {Component} from "@angular/core";

@Component({
  selector: "app-demo-date-formats",
  template: `
    <table>
      <tr><th>plain</th>      <td>{{now}}</td></tr>
      <tr><th>date</th>       <td>{{now | date}}</td></tr>
      <tr><th>short</th>      <td>{{now | date:'short'}}</td></tr>
      <tr><th>shortTime</th>  <td>{{now | date:'shortTime'}}</td></tr>
      <tr><th>shortDate</th>  <td>{{now | date:'shortDate'}}</td></tr>
      <tr><th>medium</th>     <td>{{now | date:'medium'}}</td></tr>
      <tr><th>mediumTime</th> <td>{{now | date:'mediumTime'}}</td></tr>
      <tr><th>mediumDate</th> <td>{{now | date:'mediumDate'}}</td></tr>
      <tr><th>custom</th>     <td>{{now | date:'yyyy-MM-dd HH:mm Z'}}</td></tr>
    </table>
  `,
  styles: [`table {width:40rem;} th {text-align: left;}`]
})
export class DemoDateFormatsComponent {
  now = new Date();
}
