import {Component} from "@angular/core";

@Component({
  selector: "app-demo-json",
  template: `
    <pre>obj: {{obj | json}}</pre>
  `,
  styles: []
})
export class DemoJsonComponent {
  obj: any = {
    name: "Justin Time",
    age: 42,
    phone: [
      "0731-123456", "0730-112233"
    ],
    address: {
      street: '17 Reboot Lane',
      postCode: 'USB C',
      city: 'Perl Lake'
    }
  };
}
