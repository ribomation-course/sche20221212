import {Component} from "@angular/core";

@Component({
  selector: "app-demo-ngclass",
  template: `
    <p [ngClass]="css">Foobar strikes again. CSS={{css}}</p>
    <p [ngClass]="{'first':num%2==1}">Tjolla Hopp. NUM={{num}}</p>
    <button type="button" (click)="toggle()">Toggle CSS</button>
    <button type="button" (click)="increment()">Increment NUM</button>
  `,
  styles: [`
    .first {
      background-color: darkgreen;
    }
    .second {
      background-color: orangered;
      font-variant: all-small-caps;
    }
    p {
      color: white;
      background-color: darkblue;
    }
  `]
})
export class DemoNgclassComponent {
  css: string = "first";
  num: number = 42;
  toggle(): void {
    this.css = (this.css == "first") ? "second" : "first";
  }
  increment(): void {
    this.num++;
  }
}
