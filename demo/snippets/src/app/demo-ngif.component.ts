import {Component} from "@angular/core";

@Component({
  selector: "app-demo-ngif",
  template: `
    <p>
      <button type="button" (click)="toggleName()">Toggle Name</button>
      <span *ngIf="name">The name is {{name}}</span>
      <span *ngIf="!name">Has no name</span>
    </p>
  `,
  styles: []
})
export class DemoNgifComponent {
  name: string = null;
  toggleName(): void {
    this.name = this.name ? null : "Anna Conda";
  }
}
