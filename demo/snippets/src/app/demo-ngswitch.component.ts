import {Component} from "@angular/core";

interface Shape {
  type: string;
  a: number;
  b?: number;
}

@Component({
  selector: "app-demo-ngswitch",
  template: `
    <div [ngSwitch]="shapes[idx].type">
      <div *ngSwitchCase="'rect'">
        Rectangle: width: {{shapes[idx].a}}, heigth: {{shapes[idx].b}}
      </div>
      <div *ngSwitchCase="'circ'">
        Circle: radius: {{shapes[idx].a}}
      </div>
      <div *ngSwitchCase="'tria'">
        Triangle: base: {{shapes[idx].a}}, heigth: {{shapes[idx].b}}
      </div>
    </div>
    <button type="button" (click)="toggle()">Change Shape Type</button>
  `,
  styles: []
})
export class DemoNgswitchComponent {
  shapes: Shape[] = [
    {type: 'rect', a: 10, b: 5},
    {type: 'circ', a: 4},
    {type: 'tria', a: 8, b: 4},
  ];
  idx: number = 0;
  toggle(): void {
    this.idx = (this.idx + 1) % this.shapes.length;
  }
}
