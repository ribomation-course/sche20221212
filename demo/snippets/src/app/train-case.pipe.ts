import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'Train_Case'
})
export class TrainCasePipe implements PipeTransform {
  transform(value: string, args?: any): any {
    if (!value) return '';
    return value.replace(/\s+/g, '_');
  }
}
