# Installation Instructions

In order to participate and perform the programming exercises of the course, 
you need to have the following installed.

* Zoom Client
    - https://us02web.zoom.us/download
    - [Video/Audio Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)
* GIT Client
  - https://git-scm.com/downloads
* A BASH terminal, such as the GIT client terminal
* NodeJS / NPM
  - https://nodejs.org/en/download/
* Angular CLI
  - `npm install -g @angular/cli`
* A decent IDE, such as any of
    * MicroSoft Visual Code
        - https://code.visualstudio.com/
    * JetBrains WebStorm
        - https://www.jetbrains.com/webstorm/download
* A modern browser, such as any of
  - [Google Chrome](https://www.google.com/chrome/browser/desktop/index.html)
  - [Mozilla FireFox](https://www.mozilla.org/sv-SE/firefox/new/)
  - [Microsoft Edge](https://www.microsoft.com/sv-se/windows/microsoft-edge)
