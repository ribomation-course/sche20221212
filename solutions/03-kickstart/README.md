# ng kick-start project

## pnpm
Install `pnpm` by following the instructions at https://pnpm.io/installation
The easiest would be to use npm

    npm install --global pnpm

## Create the ng project

    pnpm --package @angular/cli dlx ng new 03-kickstart --routing true --style scss --package-manager pnpm

## Install & configure the backend server

Install json-server as a dev-dep

    pnpm add -D json-server

Configuration

    mkdir -p src-server/assets/img
    cp ../../start-code/kick-start/db.json src-server/
    cp ../../start-code/kick-start/jpg/* src-server/assets/img/

## Edit `package.json`
Replace the content of the `scripts` entry with

    "server": "pnpm exec json-server --static src-server/assets --watch src-server/db.json",
    "client": "pnpm exec ng serve --open"

Then launch both the server and the client

    pnpm run server
    pnpm run client

## Generate artifacts

### Domain

    pnpm exec ng g i domain/product

### Services

    pnpm exec ng g s services/products
    pnpm exec ng g s services/basket

### Widgets

    pnpm exec ng g c widgets/navbar --type widget
    pnpm exec ng g c widgets/basket --type widget

### Pages

    pnpm exec ng g c pages/welcome --type page
    pnpm exec ng g c pages/products --type page
    pnpm exec ng g c pages/shopping-basket --type page

## Edit artifacts

### Interface: Product

    export interface Product {
        id: string;
        name: string;
        price: number;
        image: string;
    }

### Service: Products

In `app.module.ts`, insert `HttpClientModule` last in the imports section and import the TS module as well.

Replace the content of the class with the snippet below and import all dependecies.

    private readonly baseUrl     = 'http://localhost:3000'
    private readonly productsUrl = this.baseUrl + '/products';
    
    constructor(private http: HttpClient) {}
    
    findAll(): Observable<Product[]> {
        return this.http
            .get<Product[]>(this.productsUrl)
            .pipe( 
                map((list: any) => list.map((p: any) => { 
                    p.image = this.baseUrl + p.image; 
                    return p;
        })));
    }

### Service: Basket

    cp ../../start-code/kick-start/ts/basket.service.ts src/app/services

### Widget: Navbar

In `navbar.widget.ts`, insert the snippet below

    export interface Link {
        name: string;
        uri: string;
    }

    @Component({
        selector: 'navbar',
        templateUrl: './navbar.widget.html',
        styleUrls: ['./navbar.widget.scss']
    })
    export class NavbarWidget {
        readonly links: Link[] = [
            {name: 'Home', uri: '/home'},
            {name: 'Products Listing', uri: '/products'},
            {name: 'Shopping Basket', uri: '/shopping-basket'},
        ]; 
    }

In `navbar.widget.html`, insert the snippet below

    <ul>
      <li *ngFor="let lnk of links">
        <a [routerLink]="lnk.uri" routerLinkActive="active">
          {{lnk.name | titlecase}}
        </a>
      </li>
    </ul>

Copy the styles

    cp ../../start-code/kick-start/scss/navbar.widget.scss src/app/widgets/navbar

### Widget: Basket

In `basket.widget.ts` insert the snippet below and import the dependency

    @Component({
        selector: 'basket-widget',
        templateUrl: './basket.widget.html',
        styleUrls: ['./basket.widget.scss']
    })
    export class BasketWidget {
        constructor(public basketSvc: BasketService) {}
    }

Copy html and styles

    cp ../../start-code/kick-start/html/basket.widget.html src/app/widgets/basket
    cp ../../start-code/kick-start/scss/basket.widget.scss src/app/widgets/basket

### Copy the global styles

    cp ../../start-code/kick-start/scss/styles.scss src/

### Edit the routing table

In `app-routing.module.ts`, within the `[]`, insert the snippet below and import the dependencies

    {path: 'home',            component: WelcomePage},
    {path: 'products',        component: ProductsPage},
    {path: 'shopping-basket', component: ShoppingBasketPage},
    {path: '',                redirectTo: '/home', pathMatch: 'full'},


### Edit `app.component.*`

Within `app.component.ts` replace the class content with

Within `app.component.html` replace the class content with

    <section>
        <header>
            <h1>Silly ng Shopping Store</h1>
        </header>
        <nav> 
            <navbar></navbar>
            <basket-widget></basket-widget> 
        </nav>
        <main>
            <router-outlet></router-outlet>
        </main>
        <footer>
            &copy; {{year}} WhateEver Software Ltd. All rights reversed.
        </footer>
    </section>

Copy the styles

    cp ../../start-code/kick-start/scss/app.component.scss src/app/

### Edit `welcome.page.*`

Within `welcome.page.ts` insert the snippet below inside the class

    readonly image = 'http://localhost:3000/img/store.jpg';

Within `welcome.page.html` replace the content with the snippet below

    <h2>Welcome to this silly shopping store!</h2>
    <img [src]="image" >

Copy the styles

    cp ../../start-code/kick-start/scss/welcome.page.scss src/app/pages/welcome

### Edit `products.page.*`

Within `products.page.ts` insert the snippet below inside the class and import the dependencies

    products$: Observable<Product[]>;
    constructor(
        public productsSvc: ProductsService,
        public basketSvc: BasketService
    ) {
        this.products$ = productsSvc.findAll();
    }

Copy html and styles

    cp ../../start-code/kick-start/html/products.page.html src/app/pages/products
    cp ../../start-code/kick-start/scss/products.page.scss src/app/pages/products

### Edit `shopping-basket.page.*`

Within `shopping-basket.page.ts` insert the snippet below inside the class and import the dependencies

    constructor(public basketSvc: BasketService) {}

Copy html and styles

    cp ../../start-code/kick-start/html/shopping-basket.page.html src/app/pages/shopping-basket
    cp ../../start-code/kick-start/scss/shopping-basket.page.scss src/app/pages/shopping-basket


### Update the page title

Within `src/index.html` replace the title tag with

    <title>The Silly ng Store</title>
