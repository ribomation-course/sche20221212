import {Component} from '@angular/core';
import {BasketService} from "../../services/basket.service";
import {ProductsService} from "../../services/products.service";
import {Product} from "../../domain/product";
import {Observable} from "rxjs";

@Component({
    selector: 'app-products',
    templateUrl: './products.page.html',
    styleUrls: ['./products.page.scss']
})
export class ProductsPage {
    products$: Observable<Product[]>;
    constructor(
        public productsSvc: ProductsService,
        public basketSvc: BasketService
    ) {
        this.products$ = productsSvc.findAll();
    }
}
