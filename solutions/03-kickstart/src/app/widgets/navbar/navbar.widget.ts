import {Component} from '@angular/core';

export interface Link {
    name: string;
    uri: string;
}

@Component({
    selector: 'navbar',
    templateUrl: './navbar.widget.html',
    styleUrls: ['./navbar.widget.scss']
})
export class NavbarWidget {
    readonly links: Link[] = [
        {name: 'Home', uri: '/home'},
        {name: 'Products Listing', uri: '/products'},
        {name: 'Shopping Basket', uri: '/shopping-basket'},
    ];
}
