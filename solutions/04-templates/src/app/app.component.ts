import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    user: any = {
        name: {first: 'Anna', last: 'Conda'}
    };
    safe = false;
    value: number = 0;
    timer: any = undefined;

    toggle() {
        if (this.user) {
            this.user = undefined;
        } else {
            this.user = {
                name: {first: 'Per', last: 'Silja'}
            };
        }
    }

    start() {
        this.value++
        this.timer = setInterval(() => {
            this.value++
        }, 1000);
    }

    stop() {
        clearInterval(this.timer);
        this.timer = undefined;
    }

    clear() {
        this.value = 0;
    }
}
