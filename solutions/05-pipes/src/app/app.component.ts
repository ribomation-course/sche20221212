import {Component} from '@angular/core';
import {Product} from "./domain/product";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    readonly DAY = 24 * 3600 * 1000;
    asc: boolean = true;

    products: Product[] = [
        {
            name: 'Apple', price: 12.34, count: 2.5, service: false,
            updated: new Date()
        },
        {
            name: 'Banana', price: 1234.56, count: 5.5, service: true,
            updated: new Date(Date.now() + 5 * this.DAY)
        },
        {
            name: 'Orange', price: 5555.55, count: 12.5, service: false,
            updated: new Date(Date.now() - 5 * this.DAY)
        },
        {
            name: 'Pine', price: 9999.99, count: 92.5, service: true,
            updated: new Date(Date.now() + 15 * this.DAY)
        },
        {
            name: 'KiwiKiwiKiwi', price: 123.456, count: 102.15, service: false,
            updated: new Date(Date.now() - 15 * this.DAY)
        },
    ];

    sort() {
        this.products = this.products
            .sort((lhs, rhs) => {
                return this.asc ? lhs.price - rhs.price : rhs.price - lhs.price
            });
        this.asc = !this.asc;
    }
}
