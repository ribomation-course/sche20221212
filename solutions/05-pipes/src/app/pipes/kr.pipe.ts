import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'kr'
})
export class KrPipe implements PipeTransform {
    transform(value: number | string, oren: boolean = false): string {
        let result: string;
        result = (+value).toLocaleString('sv-SE', {
            useGrouping: true,
            minimumFractionDigits: oren ? 2 : 0,
            maximumFractionDigits: oren ? 2 : 0
        })
        result += ' kr';
        return result.replace(/\s+/g, '\xA0');
    }
}
