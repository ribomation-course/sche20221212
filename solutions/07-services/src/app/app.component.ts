import {Component} from '@angular/core';
import {LoggerService} from "./services/logger.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private logger: LoggerService) {
    logger.print('App started')
  }

  msg(txt: string = 'Tjabba Habba') {
    this.logger.print(txt)
  }
}
