import {Component} from '@angular/core';
import {Product} from "./domain/product";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  products: Product[] = [
    {name: 'Apple Fruit', price: 12},
    {name: 'Angular Book', price: 42},
    {name: 'Laptop Computer', price: 79},
  ];

  result: any;

  onSaved(payload: any) {
    this.result = payload;
    console.log('payload: %o', payload)
  }
}
