import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {EditableTextWidget} from "./widgets/editable/editable-text/editable-text.widget";
import {EditableNumberWidget} from "./widgets/editable/editable-number/editable-number.widget";
import {FormsModule} from "@angular/forms";
import {AbstractEditable} from "./widgets/editable/abstract-editable";

@NgModule({
  declarations: [
    AppComponent,
      EditableTextWidget,
      EditableNumberWidget,
      AbstractEditable
  ],
  imports: [
    BrowserModule,
   FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
