import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Product} from "../domain/product";

@Injectable({providedIn: 'root'})
export class ProductsService {
    private readonly url = 'http://localhost:3000/products';

    constructor(private http: HttpClient) {
    }

    findAll(): Observable<Product[]> {
        return this.http.get<Product[]>(this.url);
    }

    findById(id:number): Observable<Product> {
        return this.http.get<Product>(`${this.url}/${id}`);
    }
}
