import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from "@angular/router";

import {AppComponent} from './app.component';
import {ROUTES} from './routes'

import {HomePage} from "./pages/home/home.page";
import {AboutPage} from './pages/about/about.page';
import {ListPage} from './pages/list/list.page';
import {NotFoundPage} from './pages/not-found/not-found.page';
import { NavbarWidget } from './widgets/navbar/navbar.widget';
import {HttpClientModule} from "@angular/common/http";
import { ShowPage } from './pages/show/show.page';

@NgModule({
    declarations: [
        AppComponent,
        HomePage,
        AboutPage,
        ListPage,
        NotFoundPage,
        NavbarWidget,
        ShowPage
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(ROUTES),
        HttpClientModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
