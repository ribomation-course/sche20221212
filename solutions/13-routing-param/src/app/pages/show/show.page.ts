import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {ProductsService} from "../../services/products.service";
import {Product} from "../../domain/product";

@Component({
    selector: 'app-show',
    templateUrl: './show.page.html',
    styleUrls: ['./show.page.css']
})
export class ShowPage implements OnInit {
    product: Product | undefined = undefined;

    constructor(
        private productSvc: ProductsService,
        private route: ActivatedRoute,
        private titleSvc: Title
    ) {}

    ngOnInit(): void {
        const snap = this.route.snapshot;
        const id = Number(snap.paramMap.get('id'));
        const title = snap.data['title'];

        this.productSvc.findById(id)
            .subscribe(obj => {
                this.product = obj;
                this.titleSvc.setTitle(`${title}: ID=${id}, Name=${this.product.name}`);
            })
    }

}
