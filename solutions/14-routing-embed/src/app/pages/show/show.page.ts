import {Component, OnDestroy} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {ProductsService} from "../../services/products.service";
import {Product} from "../../domain/product";
import {Subscription} from "rxjs";

@Component({
    selector: 'app-show',
    templateUrl: './show.page.html',
    styleUrls: ['./show.page.css']
})
export class ShowPage implements OnDestroy {
    product: Product | undefined = undefined;
    subs: Subscription | undefined = undefined;

    constructor(
        private productSvc: ProductsService,
        private route: ActivatedRoute,
        private titleSvc: Title
    ) {
        const id = Number(route.snapshot.paramMap.get('id'));
        this.subs = productSvc.findById(id)
            .subscribe(obj => {
                this.product = obj;
                titleSvc.setTitle(`Product ID=${id}, Name=${this.product.name}`);
            })
    }

    ngOnDestroy(): void {
        if (this.subs) {
            this.subs.unsubscribe();
        }
    }
}
