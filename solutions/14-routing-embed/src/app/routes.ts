import {Route} from "@angular/router";
import {HomePage} from "./pages/home/home.page";
import {AboutPage} from "./pages/about/about.page";
import {ListPage} from "./pages/list/list.page";
import {NotFoundPage} from "./pages/not-found/not-found.page";
import {ShowPage} from "./pages/show/show.page";
import {ListWithDetailPage} from "./pages/list-with-detail/list-with-detail.page";
import {DetailPanel} from "./pages/list-with-detail/detail/detail.panel";

export const ROUTES: Route[] = [
    {path: 'home', component: HomePage, data: {title: 'Home'}},
    {path: 'about', component: AboutPage, data: {title: 'About...'}},
    {path: 'list', component: ListPage, data: {title: 'Product List'}},
    {path: 'show/:id', component: ShowPage},

    {
        path: 'list-detail',
        component: ListWithDetailPage,
        data: {title: 'List and Detail'},
        children: [
            {path: 'detail/:id', component: DetailPanel}
        ]
    },

    {path: '', redirectTo: '/home', pathMatch: 'full'},
    {path: '**', component: NotFoundPage},
];

