import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';

type GuardRetType = Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree;

@Injectable({providedIn: 'root'})
export class AlternatingGuard implements CanActivate {
    shouldGrant = false;

    constructor(private router: Router) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): GuardRetType {
        const granted = this.shouldGrant;
        console.log('[ALT] granted -> %o', granted)
        this.shouldGrant = !this.shouldGrant;

        if (granted) return true;

        this.router.navigate(['/home']).then(_ => true);
        return false;
    }
}
