import {Component} from '@angular/core';
import {formatDate} from "@angular/common";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    baseDate = new Date();
    offset = 1;

    toHtmlDate(d: Date) {
        return formatDate(d, 'yyyy-MM-dd', 'en');
    }

    fromHtmlDate(s: string) {
        this.baseDate = new Date(s);
    }

    get nextDate(): Date {
        const DAY_MS = 24 * 3600 * 1000;
        const ts = this.baseDate.getTime();
        return new Date(ts + this.offset * DAY_MS);
    }
}
