import {Component} from '@angular/core';
import {User} from "./domain/user";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styles: ['']
})
export class AppComponent {
    user: User = {name: '', age: 0};

    onSubmit(data: User) {
        console.log('[app] data: %o', data);
        this.user = data;
    }
}
