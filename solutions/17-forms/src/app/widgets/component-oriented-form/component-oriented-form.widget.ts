import {Component, EventEmitter, Input, Output, ViewChild} from '@angular/core';
import {User} from "../../domain/user";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'component-oriented-form',
  templateUrl: './component-oriented-form.widget.html',
  styles: ['']
})
export class ComponentOrientedFormWidget {
    @Input('init') data: User | undefined;
    @Output('submitted') submitEmitter = new EventEmitter<User>();
    form: FormGroup;
    nameCtrl :FormControl;
    ageCtrl:FormControl;

    constructor() {
        this.nameCtrl = new FormControl('', [
            Validators.required,
            Validators.minLength(5),
            Validators.maxLength(20),
        ]);
        this.ageCtrl = new FormControl('', [
            Validators.required,
            Validators.min(20),
            Validators.max(80),
        ]);
        this.form = new FormGroup({
            name: this.nameCtrl,
            age: this.ageCtrl
        });
    }

    ngOnInit(): void {
        if (!!this.data) {
            this.nameCtrl.setValue(this.data.name);
            this.ageCtrl.setValue(this.data.age);
        } else {
            throw new Error('missing init user object');
        }
    }

    emitSubmit() {
        if (this.form?.valid) {
            this.submitEmitter.emit(this.form.value);
        }
    }
}
