import {Component, EventEmitter, Input, Output, ViewChild, OnInit} from '@angular/core';
import {User} from "../../domain/user";
import {FormGroup} from "@angular/forms";

@Component({
    selector: 'template-oriented-form',
    templateUrl: './template-oriented-form.widget.html',
    styles: ['']
})
export class TemplateOrientedFormWidget implements OnInit {
    @Input('init') data: User | undefined;
    @Output('submitted') submitEmitter = new EventEmitter<User>();
    @ViewChild('form') form: FormGroup | undefined;

    formData: User = {name: '', age: 0};

    ngOnInit(): void {
        if (!!this.data) {
            this.formData = Object.assign({}, this.data);
        } else {
            throw new Error('missing init user object');
        }
    }

    emitSubmit() {
        if (this.form?.valid) {
            this.submitEmitter.emit(this.formData);
        }
    }
}
