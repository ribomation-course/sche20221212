import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from "@angular/router";

import {AppComponent} from './app.component';
import {ROUTES} from './routes'

import {HomePage} from "./pages/home/home.page";
import {AboutPage} from './pages/about/about.page';
import {ListPage} from './pages/list/list.page';
import {NotFoundPage} from './pages/not-found/not-found.page';
import { NavbarWidget } from './widgets/navbar/navbar.widget';
import {HttpClientModule} from "@angular/common/http";
import { ShowPage } from './pages/show/show.page';
import { ListWithDetailPage } from './pages/list-with-detail/list-with-detail.page';
import { DetailPanel } from './pages/list-with-detail/detail/detail.panel';
import { LoginPage } from './pages/login/login.page';
import {FormsModule} from "@angular/forms";

@NgModule({
    declarations: [
        AppComponent,
        HomePage,
        AboutPage,
        ListPage,
        NotFoundPage,
        NavbarWidget,
        ShowPage,
        ListWithDetailPage,
        DetailPanel,
        LoginPage,
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(ROUTES),
        HttpClientModule,
        FormsModule,
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
