import {Component} from '@angular/core';
import {config} from '../config/config'

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    mode = config.mode;
    robot = config.robotUrl;
}
