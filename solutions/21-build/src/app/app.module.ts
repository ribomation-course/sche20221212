import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {NavbarWidget} from './widgets/navbar/navbar.widget';
import {BasketWidget} from './widgets/basket/basket.widget';
import {WelcomePage} from './pages/welcome/welcome.page';
import {ProductsPage} from './pages/products/products.page';
import {ShoppingBasketPage} from './pages/shopping-basket/shopping-basket.page';
import {HttpClientModule} from "@angular/common/http";

@NgModule({
    declarations: [
        AppComponent,
        NavbarWidget,
        BasketWidget,
        WelcomePage,
        ProductsPage,
        ShoppingBasketPage
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
