import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map, Observable} from "rxjs";
import {Product} from "../domain/product";

@Injectable({providedIn: 'root'})
export class ProductsService {
    private readonly baseUrl = 'http://localhost:3000'
    private readonly productsUrl = this.baseUrl + '/products';

    constructor(private http: HttpClient) {
    }

    findAll(): Observable<Product[]> {
        return this.http
            .get<Product[]>(this.productsUrl)
            .pipe(
                map((list: any) => list.map((p: any) => {
                    p.image = this.baseUrl + p.image;
                    return p;
                })));
    }
}
