import {Component, ViewChild} from '@angular/core';
import {FormGroup} from "@angular/forms";
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";

interface LoginData {
    username: string;
    password: string;
}

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.css']
})
export class LoginPage {
    @ViewChild('loginForm') loginForm: FormGroup | undefined;

    login: LoginData = {username: '', password: ''};
    message: string | undefined;

    constructor(
        private authSvc: AuthService, 
        private router: Router
    ) {
    }

    onSubmit() {
        this.authSvc
            .login(this.login.username, this.login.password)
            .subscribe(successful => {
                console.log('login --> %o', successful);
                if (successful) {
                    this.router
                        .navigate(['/protected'])
                        .then(ok => true);
                } else {
                    this.message = 'Invalid username and/or password';
                    this.login.password = '';
                }
            })
    }

}
